#1  select maker, type, speed, hd from pc join product p on pc.model=p.model and hd<=8;
#2  select maker from pc join product p on pc.model=p.model and speed>=600;
#3  select maker from laptop join product p on laptop.model=p.model and speed<=500;
#4  select * from laptop join laptop p on laptop.model=p.model and laptop.hd=p.hd and laptop.ram=p.ram and laptop.code!=p.code;
#5  select distinct classes.country, classes.type, classes.class from classes join classes p on classes.type='bb' and p.type='bc' or classes.type='bc' and p.type='bb';
#6  select distinct p.model, p.maker from pc join product p on pc.model=p.model and price<600;
#7  select distinct p.model, p.maker from printer join product p on printer.model=p.model and price>300;
#8  select maker, pc.model, pc.price from pc join product p on pc.model=p.model;
#9  select distinct maker, pc.model, pc.price from pc join product p on pc.model=p.model;
#10 select maker, type, laptop.model, speed from laptop join product p on laptop.model=p.model and speed>600;
#11 select name, displacement from ships join classes c on ships.class=c.class;
#12 select distinct name, date from battles join outcomes o on battles.name=o.battle and result='OK';
#13 select name, country from ships join classes c on ships.class=c.class;
#14 select distinct plane, name from trip join company c on trip.ID_comp=c.ID_comp and plane='Boeing';
#15 select name, date from passenger join pass_in_trip p on passenger.ID_psg=p.ID_psg;

